package com.metlife.starteam;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LaunchProjectAnalyzer {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(LaunchProjectAnalyzer.class);

	public static void main(String[] args) throws IOException {
		
		/*
		 * arg0 --> Project root folder (e.g. /workspace/FileUploadForm)
		 * arg1 --> "REPORT"    : It would generate excel report. 
		 *  	--> "POM"       : It would generate pom.xml based on the excel data input and it will
		 * 					 	  perform cleanup activities.
		 * 		--> "REPORT-POM": It will generate the excel report, perform cleanup activities only those 
		 * 						  jars available in MAVEN, and generate pom.xml along with jar location and  
		 * 						  MAVEN dependency mapping text file.
		 * arg2 --> Excel report path (e.g. /output/excel-report/)
		 */
		
		if (args.length != 3) {
			LOGGER.error ("Invalid number of argument passed!");
			LOGGER.info ("==============================USAGE==============================");
			LOGGER.info ("arg0 --> Project root folder (e.g. /workspace-sts/FileUploadForm)");
			LOGGER.info ("arg1 --> \"REPORT\"    : It would generate excel report. ");
			LOGGER.info ("     --> \"POM\"       : It would generate pom.xml based on the excel data \n"
					+ "                       input and it will perform cleanup activities.");
			LOGGER.info("     --> \"REPORT-POM\": It will generate the excel report, perform \n"
					+ "                       cleanup activities only those jars available in maven, and \n"
					+ "                       generate pom.xml along with jar location and maven dependency \n "
					+ "                      mapping text file.");
			LOGGER.info ("==============================USAGE==============================");
			System.exit(1);
		}
				
		//Take the project root folder path and start analyze
		String rootFolder = args[0];
		String operation = args[1];
		String excelReportPath = args[2];
		
		ProjectAnalyzer projectAnalyzer = new ProjectAnalyzer();
		ExcelReportManager reportGenerator = new ExcelReportManager();
		
		List<File> filesList = null;
		List<FileDetails> fileDetailsList = null;
		List<File> jarFilesList = null;
		List<JarDetails> jarDetailsList = null;
		
		//initialize operations
		if (operation.equals("REPORT") || operation.equals("REPORT-POM")){
			filesList = projectAnalyzer.analyzeProject(rootFolder);
			fileDetailsList = ProjectAnalyzerUtil.captureFileDetails(filesList);
			jarFilesList = projectAnalyzer.analyzeProjectJars(rootFolder);
			jarDetailsList = ProjectAnalyzerUtil.captureJarDetails (jarFilesList);
		}
		
		switch (operation) {
			case "REPORT":
				LOGGER.info ("Generating excel report...");
				reportGenerator.generateExcelReport (filesList, jarDetailsList, excelReportPath);
				break;
			case "POM":
				LOGGER.info ("Generating POM, Maven Dependency Mapping and Perform cleanup...");
				jarDetailsList = reportGenerator.readJarDetailsFromExcelInventory(excelReportPath);
				fileDetailsList = reportGenerator.readFileDetailsFromExcelInventory(excelReportPath);
				POMGenerator.generatePOM (jarDetailsList, rootFolder);
				reportGenerator.generateJarDependencyMappingFile(jarDetailsList, rootFolder);
				//ProjectAnalyzerUtil.cleanUpResources (fileDetailsList, jarDetailsList);
				break;
			case "REPORT-POM":
				LOGGER.info ("Generating excel report, POM and Perform cleanup...");
				reportGenerator.generateExcelReport (filesList, jarDetailsList, excelReportPath);
				POMGenerator.generatePOM (jarDetailsList, rootFolder);
				reportGenerator.generateJarDependencyMappingFile(jarDetailsList, rootFolder);
				ProjectAnalyzerUtil.cleanUpResources (fileDetailsList, jarDetailsList);
				break;
			default:
				LOGGER.error ("The requested operation can not be performed!");
				break;
		}
		
		LOGGER.info ("Completed! Please check the report:" + excelReportPath);
		
	}
}
