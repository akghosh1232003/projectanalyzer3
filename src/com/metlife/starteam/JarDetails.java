package com.metlife.starteam;

public class JarDetails {
	
	private String jarName;
	private String jarVersion;
	private String jarLocation;
	private String jarChecksum;
	private boolean isJarRequired;
	private MavenDependencyMetadata dependencyMetadata;
	
	public String getJarName() {
		return jarName;
	}
	public void setJarName(String jarName) {
		this.jarName = jarName;
	}
	public String getJarVersion() {
		return jarVersion;
	}
	public void setJarVersion(String jarVersion) {
		this.jarVersion = jarVersion;
	}
	public String getJarLocation() {
		return jarLocation;
	}
	public void setJarLocation(String jarLocation) {
		this.jarLocation = jarLocation;
	}
	public String getJarChecksum() {
		return jarChecksum;
	}
	public void setJarChecksum(String jarChecksum) {
		this.jarChecksum = jarChecksum;
	}
	public boolean isJarRequired() {
		return isJarRequired;
	}
	public void setJarRequired(boolean isJarRequired) {
		this.isJarRequired = isJarRequired;
	}
	public MavenDependencyMetadata getDependencyMetadata() {
		return dependencyMetadata;
	}
	public void setDependencyMetadata(MavenDependencyMetadata dependencyMetadata) {
		this.dependencyMetadata = dependencyMetadata;
	}
	
}
