package com.metlife.starteam;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.codehaus.plexus.util.WriterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class POMGenerator {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ExcelReportManager.class);
	
	public static void generatePOM (List<JarDetails> jarDetails, String location) {
		Writer w = null;
		MavenXpp3Writer mavenXpp3Writer = new MavenXpp3Writer();
		
		try {
			w = WriterFactory.newWriter (new File (location + "pom.xml"), "UTF-8");
			
			Model model = new Model();
			model.setGroupId("com.metlife");
			model.setArtifactId("project-analyzer");
			model.setVersion("1.0");
			model.setModelVersion("4.0.0");
			
			List<Dependency> dependencies = new ArrayList<Dependency>();
			
			for (int jarCount = 0; jarCount<jarDetails.size(); jarCount++) {
				Dependency dependency = new Dependency();
				dependency.setGroupId(jarDetails.get(jarCount).getDependencyMetadata().getGroupId());
				dependency.setArtifactId(jarDetails.get(jarCount).getDependencyMetadata().getArtifactId());
				dependency.setVersion(jarDetails.get(jarCount).getDependencyMetadata().getVersion());
				
				dependencies.add(dependency);
			}
			model.setDependencies(dependencies);
			mavenXpp3Writer.write(w, model);
			
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e.getMessage(), e);
		} catch (FileNotFoundException e) {
			LOGGER.error(e.getMessage(), e);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				if (w != null)
					w.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
	}
}
